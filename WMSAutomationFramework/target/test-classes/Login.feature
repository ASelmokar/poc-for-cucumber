#Author: Amruta.Selmokar@Katalysttech.com
Feature: WMS Application SignIn 
	In order to login in WMS Application user should have to enter valid User id and login details

Background: 
	Given I have navigated to the WMS Application 
	
	#@invalidlogin
	#Scenario: Test User should not able to grant the system with invalid credential
	# When I enter a credentials for "InvalidUser"
	#And I can click sign in
	#Then I am notified Incorrect username or password
	
@login @smoke 
Scenario: Test User should able to grant the system with valid credential 
	When I enter a credentials for WMS "WMSUserDetails" 
	And I can click log in button of WMS
	And I click on dashboard button
	Then I am granted access to the WMS Application
	
	