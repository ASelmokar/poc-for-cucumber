$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/Receiving.feature");
formatter.feature({
  "name": "Create new Receipt",
  "description": "\tCreate new new Receipt for Purchase Order ",
  "keyword": "Feature"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "I have navigated to the WMS Application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginWMSStepDef.i_have_navigated_to_the_WMS_Application()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter a credentials for WMS \"WMSUserDetails\"",
  "keyword": "When "
});
formatter.match({
  "location": "LoginWMSStepDef.i_enter_a_credentials_for_WMS(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I can click log in button of WMS",
  "keyword": "And "
});
formatter.match({
  "location": "LoginWMSStepDef.i_can_click_log_in_button_of_WMS()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on dashboard button",
  "keyword": "And "
});
formatter.match({
  "location": "LoginWMSStepDef.i_click_on_dashboard_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I am granted access to the WMS Application",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginWMSStepDef.i_am_granted_access_to_the_WMS_Application()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on Purchase order Link",
  "keyword": "Given "
});
formatter.match({
  "location": "PurchaseOrderStepDef.i_click_on_Purchase_order_Link()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on PO tab",
  "keyword": "When "
});
formatter.match({
  "location": "PurchaseOrderStepDef.i_click_on_PO_tab()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I am able to create new Purchase Order",
  "keyword": "Then "
});
formatter.match({
  "location": "PurchaseOrderStepDef.i_am_able_to_create_new_Purchase_Order()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I am able to Approve it",
  "keyword": "And "
});
formatter.match({
  "location": "PurchaseOrderStepDef.i_am_able_to_Approve_it()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I am able to validate PO has been successfully created",
  "keyword": "And "
});
formatter.match({
  "location": "PurchaseOrderStepDef.i_am_able_to_validate_PO_has_been_successfully_created()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Test User should able to create new Receipt for Purchase Order",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@PO"
    }
  ]
});
formatter.step({
  "name": "I click on Purchase Link for receipt",
  "keyword": "Given "
});
formatter.match({
  "location": "ReceiptStepDef.i_click_on_Purchase_Link_for_receipt()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on Receiving tab",
  "keyword": "When "
});
formatter.match({
  "location": "ReceiptStepDef.i_click_on_Receiving_tab()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I am able to create new Receipt for generated Purchase order",
  "keyword": "Then "
});
formatter.match({
  "location": "ReceiptStepDef.i_am_able_to_create_new_Receipt_for_generated_Purchase_order()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I am able to validate receipt has been successfully created",
  "keyword": "And "
});
formatter.match({
  "location": "ReceiptStepDef.i_am_able_to_validate_receipt_has_been_successfully_created()"
});
formatter.result({
  "status": "passed"
});
});