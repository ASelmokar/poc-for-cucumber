package Com.katalyst.WMS_stepdef;


import com.katalyst.steps.PurchaseOrderSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class PurchaseOrderStepDef {
	
	@Steps
	PurchaseOrderSteps  POSteps;
	
	@Given("^I click on Purchase order Link$")
	public void i_click_on_Purchase_order_Link() {
		POSteps.clickOnPOLink();
	}


	@When("^I click on PO tab$")
	public void i_click_on_PO_tab() {
		POSteps.clickOnPOTab();
	}

	@Then("^I am able to create new Purchase Order$")
	public void i_am_able_to_create_new_Purchase_Order() throws Exception {
		POSteps.createNewPurchaseOrder();
	}

	@Then("^I am able to Approve it$")
	public void i_am_able_to_Approve_it() {
		POSteps.approveCreatedPurchaseOrder();
	}

	@Then("^I am able to validate PO has been successfully created$")
	public void i_am_able_to_validate_PO_has_been_successfully_created() {
		POSteps.validatePOCreated();
	}


}
