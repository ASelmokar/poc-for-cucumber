package Com.katalyst.WMS_stepdef;




import com.katalyst.steps.SignInStepsWMS;
import com.katalyst.utils.Log;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class LoginWMSStepDef {
	@Steps
	SignInStepsWMS signinSteps;
	
	@Given("^I have navigated to the WMS Application$")
	public void i_have_navigated_to_the_WMS_Application() {
		signinSteps.openURL();
		Log.info("I'm on WMS page");
	}

	@When("^I enter a credentials for WMS \"([^\"]*)\"$")
	public void i_enter_a_credentials_for_WMS(String arg1) {
		signinSteps.loginToWMS(arg1);
	}

	@When("^I can click log in button of WMS$")
	public void i_can_click_log_in_button_of_WMS() {
		signinSteps.clickLoginBtnWMS();
	}
	
	@When("^I click on dashboard button$")
	public void i_click_on_dashboard_button() {
		signinSteps.dashboardStepWMS();
	}

	@Then("^I am notified Incorrect username or password$")
	public void i_am_notified_Incorrect_username_or_password() {
	
	}

	@Then("^I am granted access to the WMS Application$")
	public void i_am_granted_access_to_the_WMS_Application() {
	    
		
	}





}
