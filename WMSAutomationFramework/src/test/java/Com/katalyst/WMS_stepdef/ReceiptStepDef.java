package Com.katalyst.WMS_stepdef;



import com.katalyst.steps.ReceiptSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class ReceiptStepDef {
	
	@Steps
	ReceiptSteps  ReceiptSteps;
	
	
	@Given("^I click on Purchase Link for receipt$")
	public void i_click_on_Purchase_Link_for_receipt() {
		ReceiptSteps.clickOnPurchaseLink();
	}
	
	@When("^I click on Receiving tab$")
	public void i_click_on_Receiving_tab() {
		ReceiptSteps.clickOnReceivingTab();
	}


	@Then("^I am able to create new Receipt for generated Purchase order$")
	public void i_am_able_to_create_new_Receipt_for_generated_Purchase_order() throws Exception {
		ReceiptSteps.createReceiptForPO();
		ReceiptSteps.approveCreatedPurchaseOrderReceipt();
	}
	

	@Then("^I am able to validate receipt has been successfully created$")
	public void i_am_able_to_validate_receipt_has_been_successfully_created() {
		ReceiptSteps.validatePOCreated();
	}


}
