package Com.meganexus.runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import net.serenitybdd.cucumber.CucumberWithSerenity;


@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = { "src\\test\\resources\\Receiving.feature"},
//tags = { "@PO" }, 
glue = {"Com.katalyst.WMS_stepdef"},
monochrome = true,
//dryRun = true,
plugin = { "pretty", "html:target/cucumber-reports","json:target/cucumber.json", "rerun:rerun.txt" })

public class TestRunner{
	
}
