#Author: Amruta.Selmokar@Katalysttech.com
Feature: Create new Receipt 
	Create new new Receipt for Purchase Order 

Background: 
	Given I have navigated to the WMS Application 
	When I enter a credentials for WMS "WMSUserDetails" 
	And I can click log in button of WMS 
	And I click on dashboard button 
	Then I am granted access to the WMS Application 
	
	Given I click on Purchase order Link 
	When I click on PO tab 
	Then I am able to create new Purchase Order 
	And I am able to Approve it 
	And I am able to validate PO has been successfully created
	
	
@PO
Scenario: Test User should able to create new Receipt for Purchase Order 

	Given I click on Purchase Link for receipt
	When I click on Receiving tab 
	Then I am able to create new Receipt for generated Purchase order 
	And I am able to validate receipt has been successfully created