  #Author: Amruta.Selmokar@Katalysttech.com
Feature: Create new Purchase Order 
	Create new Purchase Order with valid User id and login details

Background: 
	Given I have navigated to the WMS Application 
	When I enter a credentials for WMS "WMSUserDetails" 
	And I can click log in button of WMS 
	And I click on dashboard button 
	Then I am granted access to the WMS Application 
	
	
@PO @smoke 
Scenario: Test User should able to create new Purchase Order 

	Given I click on Purchase order Link 
	When I click on PO tab 
	Then I am able to create new Purchase Order 
	And I am able to Approve it 
	And I am able to validate PO has been successfully created
	