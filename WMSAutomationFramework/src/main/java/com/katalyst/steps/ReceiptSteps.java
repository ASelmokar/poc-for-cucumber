package com.katalyst.steps;

import com.katalyst.pageobject.ReceiptPage;
import com.katalyst.pageobject.WMSHomePage;

public class ReceiptSteps extends WMSSteps {
	
	ReceiptPage ReceiptPage;
	WMSHomePage homePage;
	
	

	public void clickOnPurchaseLink() {
		ReceiptPage.clickOnPurchaseLink();
	}



	public void clickOnReceivingTab() {
		ReceiptPage.clickOnReceivingTab();
		
	}

	public void createReceiptForPO() throws Exception {
		ReceiptPage.createReceiptForPO();
		
	}



	public void approveCreatedPurchaseOrderReceipt() {
		ReceiptPage.approveCreatedPurchaseOrderReceipt();
		
	}

	public void validatePOCreated() {
		ReceiptPage.validatePOCreated();
		
	}



	
}
