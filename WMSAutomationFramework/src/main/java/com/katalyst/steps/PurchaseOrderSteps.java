package com.katalyst.steps;

import com.katalyst.pageobject.PurchaseOrderPage;
import com.katalyst.pageobject.SignInPageWMS;
import com.katalyst.pageobject.WMSHomePage;

public class PurchaseOrderSteps extends WMSSteps {
	PurchaseOrderPage POPage;
	WMSHomePage homePage;
	
	public void clickOnPOLink() {
		POPage.clickOnPOLink();
		
	}

	public void clickOnPOTab() {
		
		POPage.clickOnPOTab();
	}

	public void createNewPurchaseOrder() throws Exception {
		
		POPage.createNewPurchaseOrder();
	}

	public void approveCreatedPurchaseOrder() {
		
		POPage.approveCreatedPurchaseOrder();
		
	}

	public void validatePOCreated() {
		POPage.validatePOCreated();
		
	}
	
	
	
	
	
	
}
