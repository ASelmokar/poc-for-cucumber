package com.katalyst.steps;



import com.katalyst.pageobject.SignInPageWMS;
import com.katalyst.pageobject.WMSHomePage;
import com.katalyst.utils.Log;

import net.thucydides.core.annotations.Step;

public class SignInStepsWMS extends WMSSteps {
	SignInPageWMS signInPageWMS;
	WMSHomePage homePage;
	
	@Step
	public void openURL() {
		signInPageWMS.open();
		Log.info("Launching the browser");
		getDriver().manage().window().maximize();
	}

	

	@Step
	public void loginToWMS(String WMSUserDetails) {
		
		signInPageWMS.login(WMSUserDetails);

	}

	@Step
	public void clickLoginBtnWMS() {
		signInPageWMS.clickOnSignInBtnWMS();
	}

	
	@Step
	public void dashboardStepWMS() {
		signInPageWMS.dashboardStepWMS();
	}

	@Step
	public void logOutWMS() {
		homePage.clickOnLogoutBtnWMS();
	}

	@Step
	public void validateErrorMessageWMS() {
		signInPageWMS.loginFailedErrorMessageWMS();

	}

}

