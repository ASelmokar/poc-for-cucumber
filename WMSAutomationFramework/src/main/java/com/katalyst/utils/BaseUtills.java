package com.katalyst.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;



import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class BaseUtills extends PageObject {
	JavascriptExecutor executor;
	static WebDriver driver;
	WebDriverWait wait;
	public static String currentDir = System.getProperty("user.dir");
	public static String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());


	public void scrollToClickAnelement(WebElementFacade element) {
		executor = (JavascriptExecutor) getDriver();
		executor.executeScript("arguments[0].click();", element);
	}

	public void scrollToFocusAnelement(WebElementFacade element) {
		executor = (JavascriptExecutor) getDriver();
		executor.executeScript("document.getElementById('elementid').focus();");
	}

	public boolean isClickable(WebElementFacade element) throws ElementClickInterceptedException {
		try {
			wait.until(ExpectedConditions.elementToBeClickable(element));
			return true;
		} catch (Exception ElementClickInterceptedException) {
			return false;
		}
	}
	
	public static boolean isDisplayed(WebElement element) throws Exception
	{
		boolean visible;
		try{
			element.isDisplayed();
			((JavascriptExecutor)driver).executeScript("arguments[0].style.border='2px solid green'", element);
			visible = true;
		}
		catch(Exception e)
		{
			visible = false;
		}
		return visible;
	}


	public boolean isElementVisible(WebElementFacade element) throws ElementNotVisibleException {

		try {
			executor = (JavascriptExecutor) getDriver();
			wait = new WebDriverWait(getDriver(), 60000);
			executor.executeScript("arguments[0].scrollIntoView(true);", element);
			wait.until(ExpectedConditions.visibilityOf(element));
			return true;
		} catch (Exception ElementNotVisibleException) {
			return false;
		}
	}

	public String findInDDL(List<WebElementFacade> DDL_values, String key) {
		List<WebElementFacade> list = DDL_values;
		String KEY = null;

		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).getText().equals(key)) {

				KEY = list.get(i).getText();
				break;
			}
		}
		return KEY;
	}
	

	public void selectFromDDL(List<WebElementFacade> DDL_values, String name) {
		try {
			if (findInDDL(DDL_values, name).equals(name)) {
				List<WebElementFacade> list = DDL_values;
				for (WebElementFacade element : list) {
					if (element.getText().equals(name)) {
						element.click();
					}
				}

			} else
				Log.info(name + "- Value Not present in DDL");
		} catch (Exception e) {
			Log.info(name + "- Value Not present in DDL.");
		}
	}

	
	public void clickCheckbox(WebElementFacade element) {
		try {
			scrollToClickAnelement(element);
			executor.executeScript("arguments[0].click();", element);
			getDriver().findElement(By.xpath("//*[@id='mat-option-30']/span")).click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
