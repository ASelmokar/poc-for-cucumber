package com.katalyst.pageobject;

import com.katalyst.utils.Log;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public class WMSHomePage extends WMSPage {

	@FindBy(xpath = "//span[contains(text(),'Logout')]")
	private WebElementFacade btn_logOut;

	public void clickOnLogoutBtnWMS() {
		try {
			waitFor(btn_logOut);
			btn_logOut.click();
			Log.info("Logout from application");
		} catch (Exception e) {
			Log.error("User must login to application first before clicking Logout");
		}

	}

	public void titelOfThePage() {
		getDriver().getTitle();
	}

	@Override
	public WebElementFacade getUniqueElement() {
		
		return btn_logOut;
	}

}
