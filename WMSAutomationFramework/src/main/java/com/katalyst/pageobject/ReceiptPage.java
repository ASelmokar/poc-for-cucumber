package com.katalyst.pageobject;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import com.katalyst.utils.BaseUtills;
import com.katalyst.utils.ExcelUtils;

import net.serenitybdd.core.pages.WebElementFacade;

public class ReceiptPage extends WMSPage {

	BaseUtills baseUtils;

	static int rowNum;

	{
		ExcelUtils.setExcelFile("ReceiptPage");
	}

	@Override
	public WebElementFacade getUniqueElement() {
		return null;

	}

	@FindBy(xpath = "//section[@class='sidebar']/ul/li/a/span[contains(text(),'Purchase')]")
	private static WebElementFacade lnk_Purchase;

	public void clickOnPurchaseLink() {

		waitFor(lnk_Purchase);
		lnk_Purchase.click();

	}

	@FindBy(xpath = "(//span[contains(text(),'Receiving')])[1]")
	private static WebElement lnk_Receiving;

	public void clickOnReceivingTab() {
		waitFor(lnk_Receiving);
		lnk_Receiving.click();
	}

	@FindBy(id = "btnAddNew")
	private static WebElement btn_AddNew;

	@FindBy(xpath = "//select[@id='SiteId']/option")
	private static List<WebElementFacade> drp_Site;

	@FindBy(xpath = "//select[@id='ddPurchaseOrderNo']")
	private static WebElementFacade drp_POno;

	@FindBy(id = "ASNId")
	private static WebElement drp_ASNno;

	@FindBy(id = "DeliveryNo")
	private static WebElement txt_Deliveryno;

	@FindBy(id = "SupplierInvoiceNo")
	private static WebElement txt_InvoiceNo;

	@FindBy(id = "ExchangeRate")
	private static WebElement txt_ExRate;

	@FindBy(id = "btnNext")
	private static WebElement btn_next;

	@FindBy(id = "spanReceiveAll")
	private static WebElement btn_ReceiveAll;

	@FindBy(id = "btnSave")
	private static WebElement btn_Save;

	@FindBy(xpath = "//h1[1]")
	private static WebElement heading_Receipt;

	@FindBy(id = "SiteId")
	private static WebElement drp_Site1;

	public void createReceiptForPO() throws Exception {

		rowNum = ExcelUtils.getRowNumber("Receipt", "Create");

		waitFor(btn_AddNew);
		btn_AddNew.click();
		waitFor(3000);

		baseUtils.selectFromDDL(drp_Site, ExcelUtils.getCellData(rowNum, ExcelUtils.getCellNumber("Site")).trim());

		// String PONumber = PurchaseOrderPage.po_no;
		String PONumber = PurchaseOrderPage.po_no;
		System.out.println("PONumber" + PONumber);

		Select select = new Select(drp_POno);
		select.selectByVisibleText(PONumber);

		String DeliverNumber = ExcelUtils.getCellData(rowNum, ExcelUtils.getCellNumber("DeliveryNo.")).trim();
		System.out.println("DeliverNumber" + DeliverNumber);

		txt_Deliveryno.sendKeys(ExcelUtils.getCellData(rowNum, ExcelUtils.getCellNumber("DeliveryNo.")).trim());

		String InvoiceNumber = ExcelUtils.getCellData(rowNum, ExcelUtils.getCellNumber("InvoiceNo.")).trim();
		System.out.println("InvoiceNumber" + InvoiceNumber);

		txt_InvoiceNo.sendKeys(ExcelUtils.getCellData(rowNum, ExcelUtils.getCellNumber("InvoiceNo.")).trim());
		waitFor(3000);
		btn_next.click();

		waitFor(10000);

		btn_ReceiveAll.click();
		waitFor(13000);

		waitFor(btn_Save);
		btn_Save.click();
		waitFor(13000);

	}

	@FindBy(id = "btnApprove")
	private static WebElement btn_Approve;


	public void approveCreatedPurchaseOrderReceipt() {
		btn_Approve.click();

	}
	
	@FindBy(xpath = "//table[@id='tblReceipt']/tbody/tr[1]/td/a[1]")
	private static WebElement txt_poReceipt;

	public void validatePOCreated() {
		waitFor(heading_Receipt);
		waitFor(3000);

		String poReceipt_no = txt_poReceipt.getText();
		System.out.println(poReceipt_no);
		((JavascriptExecutor) getDriver()).executeScript("arguments[0].style.border='3px solid green'", txt_poReceipt);

	}
}
