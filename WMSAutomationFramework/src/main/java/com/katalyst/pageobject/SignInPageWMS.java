package com.katalyst.pageobject;

import com.katalyst.utils.BaseUtills;
import com.katalyst.utils.ExcelUtils;
import com.katalyst.utils.Log;

import org.junit.Assert;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public class SignInPageWMS extends WMSPage {
	BaseUtills baseUtils;
	static int rowNum;
	{
		ExcelUtils.setExcelFile("LoginPage");
	}
	@FindBy(id = "exampleInputEmail")
	private WebElementFacade tbx_username;
	@FindBy(id = "exampleInputPassword")
	private WebElementFacade tbx_password;
	@FindBy(xpath = "//input[@type='submit']")
	private WebElementFacade btn_Signin;
	
	@FindBy(xpath="//span[contains(text(),'Dashboard')]")
	private WebElementFacade btn_dashboard;
	
	@FindBy(xpath = "//mat-error[@role='alert']")
	private WebElementFacade alert_message;
	@FindBy(xpath = "//a[@href='/dashboard']")
	private WebElementFacade dashboard_page;
	@FindBy(xpath = "//i[@class='fa icon-dashboard']//parent::h1")
	private WebElementFacade pageTitle;
	@FindBy(xpath = "//a[@href='#/manage-establishments']")
	private WebElementFacade manageEstablishmentBtn;
	@FindBy(xpath = "//input[@name='agree_to_terms']")
	private WebElementFacade agree_terms;
	@FindBy(id="btnSubmit")
	private WebElementFacade bt_submit;
	

	public void login( String WMSUserDetails) {
		rowNum = ExcelUtils.getRowNumber("TC_Details", "WMSUserDetails");
		
		String username=ExcelUtils.getCellData(rowNum,  ExcelUtils.getCellNumber("UserName")).trim();
		Log.info("Username = "+username);
		tbx_username.sendKeys(username);
		waitABit(500);
		String password=ExcelUtils.getCellData(rowNum,  ExcelUtils.getCellNumber("Password")).trim();
		Log.info("Password = "+password);
		tbx_password.sendKeys(password);
		waitABit(500);

	}

	public void clickOnSignInBtnWMS() {
		btn_Signin.click();
		//baseUtils.isElementVisible(pageTitle);
		//baseUtils.isClickable(manageEstablishmentBtn);
		waitABit(3000);
	}

	public void dashboardStepWMS() {
		btn_dashboard.click();
		//waitFor(dashboard_page);
		baseUtils.isElementVisible(pageTitle);
		Log.info("land on dashboard page");
	}

	public String errorMessage() {
		return alert_message.getText();
	}

	@Override
	public WebElementFacade getUniqueElement() {

		return btn_Signin;
	}

	@FindBy(id = "mat-error-3")
	private WebElementFacade txt_loginFailErrorMsg;

	public void loginFailedErrorMessageWMS() {
		String expected = ExcelUtils.getCellData(4, 5).trim();
		String actual = txt_loginFailErrorMsg.getText().trim();
		Assert.assertEquals(expected, actual);

	}

}
