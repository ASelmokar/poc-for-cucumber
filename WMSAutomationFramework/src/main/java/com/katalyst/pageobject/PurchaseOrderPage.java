package com.katalyst.pageobject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.katalyst.utils.BaseUtills;
import com.katalyst.utils.ExcelUtils;


import net.serenitybdd.core.pages.WebElementFacade;

public class PurchaseOrderPage  extends WMSPage {
	public static String po_no = null;
	
	BaseUtills baseUtils;
	
	static int rowNum;

	{
		ExcelUtils.setExcelFile("PurchaseOrder");
	}

	
	@Override
	public WebElementFacade getUniqueElement() {
		
		return lnk_Purchase;
	}
	
	
	
	@FindBy(xpath = "//section[@class='sidebar']/ul/li/a/span[contains(text(),'Purchase')]")
    private static WebElementFacade lnk_Purchase;
	
	
	public void clickOnPOLink() {
	    waitFor(lnk_Purchase);
		lnk_Purchase.click();
		
	}
	
	
	
	@FindBy(xpath = "//section[@class='sidebar']//following-sibling::span[text()='Purchase Order']")
	private static WebElementFacade lnk_PurchaseOrder;
	
	public void clickOnPOTab() {
		waitFor(lnk_PurchaseOrder);
		lnk_PurchaseOrder.click();

	}
	
	
	
	
	@FindBy(id = "btnAddNew")
	private static WebElementFacade btn_AddNew;
	
	@FindBy(xpath = "//a[text()='Order Details']")
	private static WebElementFacade lnk_OrderDetails;
	
	@FindBy(xpath = "//select[@id='OrderSiteId']/option")
	private static List<WebElementFacade> drp_OrderSite;

	@FindBy(xpath = "//select[@id='PoTypeId']/option")
	private static List<WebElementFacade> drp_POType;
	
	@FindBy(xpath = "//select[@id='VendorId']/option")
	private static List<WebElementFacade> drp_VendorCode;
	
	@FindBy(xpath = "//select[@id='DeliverySiteId']/option")
	private static List<WebElementFacade> drp_DeliverySite;
	
	@FindBy(id = "ExchangeRate")
	private static WebElement txt_ExchangeRate;
	
	@FindBy(id = "TransporterId")
	private static WebElement drp_Transporter;
	
	@FindBy(id = "TransporterModeId")
	private static WebElement drp_Mode;
	
	@FindBy(id = "DepartmentId")
	private static WebElement drp_Department;
	
	@FindBy(id = "StatusReason")
	private static WebElement txt_Remarks;
	
	@FindBy(id = "TaxConfigId")
	private static WebElement drp_Tax;
	
	@FindBy(id = "DiscountConfigId")
	private static WebElement drp_Discount;
	
	@FindBy(id = "AdditionalConfigId")
	private static WebElement drp_AdditionalCharges;
	
	@FindBy(id = "TaxCalculationConfigId")
	private static WebElement drp_TaxCalculations;
	
	@FindBy(id = "btnNext")
	private static WebElement btn_next;
	
	
	@FindBy(xpath = "//select[@id='ParentItemId']/option")
	private static List<WebElementFacade> drp_AddNewItem;
	
	
	@FindBy(xpath = "//input[@name='ItemDetails[0].Qty']")
	private static WebElement txt_Qty0;
	
	@FindBy(xpath = "//input[@name='ItemDetails[1].Qty']")
	private static WebElement txt_Qty1;
	
	@FindBy(xpath = "//input[@name='ItemDetails[2].Qty']")
	private static WebElement txt_Qty2;    
	
	@FindBy(xpath = "//input[@name='ItemDetails[3].Qty']")
	private static WebElement txt_Qty3; 
	
	@FindBy(xpath = "//input[@name='ItemDetails[0].Rate']")
	private static WebElement txt_Rate0;
	
	@FindBy(xpath = "//input[@name='ItemDetails[0].Rate']")
	private static WebElement txt_Rate1;
	
	@FindBy(xpath = "//input[@name='ItemDetails[0].Rate']")
	private static WebElement txt_Rate2;    
	
	@FindBy(xpath = "//input[@name='ItemDetails[0].Rate']")
	private static WebElement txt_Rate3; 
	
	
	@FindBy(id = "btnSave")
	private static WebElement btn_SaveTab2;
	
	@FindBy(id = "btnApprove")
	private static WebElement btn_Approve;
	
	
	@FindBy(xpath = "//table[@id='tblPurchaseOrder']/tbody/tr[1]/td/a")
	private static WebElement lnk_po_number;
	

	public void createNewPurchaseOrder() throws Exception {
		
		rowNum = ExcelUtils.getRowNumber("PurchaseOrder", "Create");
		
		waitFor(btn_AddNew);
		btn_AddNew.click();
		
		waitFor(3000);
		
		baseUtils.selectFromDDL(drp_OrderSite, ExcelUtils.getCellData(rowNum, ExcelUtils.getCellNumber("Order Site")).trim());
		
		baseUtils.selectFromDDL(drp_POType, ExcelUtils.getCellData(rowNum, ExcelUtils.getCellNumber("PO Type")).trim());

		baseUtils.selectFromDDL(drp_VendorCode, ExcelUtils.getCellData(rowNum, ExcelUtils.getCellNumber("VendorCode")).trim());
		
		baseUtils.selectFromDDL(drp_DeliverySite, ExcelUtils.getCellData(rowNum, ExcelUtils.getCellNumber("Delivery Site")).trim());
		
		
	
		txt_ExchangeRate.sendKeys(ExcelUtils.getCellData(rowNum, ExcelUtils.getCellNumber("Exchange Rate")).trim());
		
		btn_next.click();
		waitFor(3000);
		
		//Product Details Page
		
		baseUtils.selectFromDDL(drp_AddNewItem, ExcelUtils.getCellData(rowNum, ExcelUtils.getCellNumber("New Item")).trim());
		waitFor(500);
		txt_Qty0.clear();
		txt_Qty0.sendKeys(ExcelUtils.getCellData(rowNum, ExcelUtils.getCellNumber("Qty0")).trim());

		waitFor(500);
		txt_Qty1.clear();
		txt_Qty1.sendKeys(ExcelUtils.getCellData(rowNum, ExcelUtils.getCellNumber("Qty1")).trim());
		waitFor(500);
		
		txt_Qty2.clear();
		txt_Qty2.sendKeys(ExcelUtils.getCellData(rowNum, ExcelUtils.getCellNumber("Qty2")).trim());
		waitFor(500);
		
		txt_Qty3.clear();
		txt_Qty3.sendKeys(ExcelUtils.getCellData(rowNum, ExcelUtils.getCellNumber("Qty3")).trim());
		waitFor(1000);
		
		btn_SaveTab2.click();
		waitFor(3000);
		
	}
	public void approveCreatedPurchaseOrder() {
		btn_Approve.click();
	}
	
	@FindBy(xpath = "//h1[1]")
	private static WebElement heading_PurchaseOrder;
	
	@FindBy(xpath = "//table[@id='tblPurchaseOrder']/tbody/tr[1]/td/a[1]")
	private static WebElement txt_po;
	
	public void validatePOCreated() {
		waitFor(heading_PurchaseOrder);
		waitFor(3000);

	    po_no = txt_po.getText();
    	System.out.println(po_no);
		((JavascriptExecutor)getDriver()).executeScript("arguments[0].style.border='3px solid green'", txt_po);


	}
	
}
